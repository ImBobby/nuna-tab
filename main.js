
$(function () {
    var $autoRotateTab = $('.js-auto-rotate-tab');

    if ( !$autoRotateTab.length ) return;

    var $tabNavs = $autoRotateTab.find('.nav-tabs a');
    var tabNavsLen = $tabNavs.length;
    var speed = $autoRotateTab.data('speed');
    var counter = 1;

    window.setInterval(function () {
        if ( counter > tabNavsLen ) {
            counter = 0;
        }

        $tabNavs.eq(counter).trigger('click');
        counter++;
    }, speed);
});
